%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[11pt,a4paper]{report}

\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{adjustbox}
\usepackage{color}
\usepackage{xcolor}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{xspace}
\usepackage{listings}
\usepackage{tikz}
\usepackage[skins]{tcolorbox}
\usepackage{framed}
\usepackage{amsmath}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage[
    hidelinks,
	pdfauthor={Keoma Brun Laguna},
    pdftitle={Deterministic Networking for the Industrial IoT},
    pdfsubject={Deterministic Networking for the Industrial IoT},
    pdfkeywords={WSN, IIoT, TSCH},
]{hyperref}
\usepackage{fontspec}
\setmainfont{Old Standard TT}
\usepackage{lscape}

\usepackage[
    style=ieee,
    bibstyle=numeric,
    sorting=none,
    url=false,
    natbib=true,
    backend=biber,
    maxbibnames=5,
    giveninits=true,
    defernumbers=true,
    maxnames=99,
]{biblatex}

% biblatex
\DeclareSourcemap{
  \maps[datatype=bibtex]{
    \map{
      \step[fieldsource=author,
            match=Keoma,
            final]
      \step[fieldset=keywords, fieldvalue=keoma]
    }
  }
}
\addbibresource{mypapers.bib}
\addbibresource{brun18deterministic.bib}

\graphicspath{{figs/}}

\title{Deterministic Networking for the Industrial IoT}     
\date{}

\author{Keoma Brun-Laguna}

%----------------------------------------------------------------------------------------
%	ALIASES
%----------------------------------------------------------------------------------------

% reviewers
%\newcommand{\keoma}[1]          {\textcolor{blue}{\textbf{[Keoma]} #1}}
%\newcommand{\pascale}[1]        {\textcolor{blue}{\textbf{[Pascale]} #1}}
%\newcommand{\thomas}[1]         {\textcolor{blue}{\textbf{[Thomas]} #1}}

%\newcommand{\lorem}             {\textcolor{green}{Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.}}
%\newcommand{\todo}[1]           {\textcolor{red}{\textbf{[TODO] #1}}}

% colors
\definecolor{myblue}{RGB}{0,163,243}
\definecolor{mygrey}{RGB}{245,245,245}

% circle
\newcommand*\circled[1]{\tikz[baseline=(char.base)]{
    \node[shape=circle,draw,inner sep=1pt] (char) {#1};}}

% chapeau
\newcommand{\chapeau}[1] 		{
	\begin{tcolorbox}[
    	enhanced,
        width=5in,
    	halign=flush left,
        enlarge bottom by=0.5cm,
    	fontupper=\large\bfseries,drop fuzzy shadow southwest,
    	boxrule=0.4pt,
        colframe=mygrey!80!black,
    	colback=mygrey!10,
        arc=10pt,
        auto outer arc,
    ]
{#1}
\end{tcolorbox}
}

% textbox
\newcommand{\textbox}[1]{
	\begin{tcolorbox}[
    	enhanced,
        arc=10pt,
        auto outer arc,
        colback=white,
        boxrule=0.8pt
    ]
{#1}
\end{tcolorbox}
}

% Shortcuts

\newcommand{\etal}                 {\textit{et al.}\xspace}
\newcommand{\wsn}                  {Wireless Sensor Network\xspace}
\newcommand{\realworld}            {real-world\xspace}
\newcommand{\tbs}                  {Trace-Based Simulation\xspace}
\newcommand{\tsch}                 {Time Slotted Channel Hopping\xspace}
\newcommand{\rpi}                  {Raspberry~Pi\xspace}
\newcommand{\iot}                  {Internet of Things\xspace}
\newcommand{\iiot}                 {Industrial Internet of Things\xspace}
\newcommand{\6}                    {6TiSCH\xspace}
\newcommand{\building}             {Smart Building\xspace}
\newcommand{\agri}                 {Smart Agriculture\xspace}

% Deployments

%-- global counters

\newcommand{\totalstats}           {3M\xspace}
\newcommand{\totalmeasurements}    {32M\xspace}
\newcommand{\NUMDATASETS}          {11\xspace}
\newcommand{\NUMMOTEHOURS}         {170,037\xspace}
\newcommand{\NUMMEASUREMENTS}      {2,873,156\xspace}

%-- peach
\newcommand{\peach}                {PEACH\xspace}
\newcommand{\peachNumNodes}        {23\xspace}
\newcommand{\peachArea}            {11$km^2$\xspace}
\newcommand{\peachNUMHRNEIGHBORS}  {140,897\xspace}
\newcommand{\peachNUMSTATS}        {369,276\xspace}
\newcommand{\peachNUMPCKTS}        {693,844\xspace}

%-- snowhow
\newcommand{\snowhow}              {SnowHow\xspace}
\newcommand{\snowhowNumNodes}      {27\xspace} % CZO
\newcommand{\snowhowArea}          {100$km^2$\xspace}

%-- evalab
\newcommand{\evalab}               {EvaLab\xspace}
\newcommand{\evalabNumNodes}       {22\xspace}
\newcommand{\evalabArea}           {800$m^2$\xspace}
\newcommand{\evalabNUMSTATS}       {386,929\xspace}
\newcommand{\evalabNUMHRNEIGHBORS} {128,072\xspace}
\newcommand{\evalabNUMPCKTS}       {431,193\xspace}

%-- smartmarina
\newcommand{\smartmarina}          {SmartMarina\xspace}
\newcommand{\smartmarinaNumNodes}  {19\xspace}
\newcommand{\smartmarinaArea}      {5$km^2$\xspace}

%-- misc
\newcommand{\grizzly}              {Grizzly~Ridge\xspace}
\newcommand{\bucks}           	   {Bucks~Lake\xspace}
\newcommand{\kettle}           	   {Kettle~Rock\xspace}
\newcommand{\mercator}             {Mercator\xspace}
\newcommand{\iotlab}           	   {IoT-LAB\xspace}
\newcommand{\openwsn}              {OpenWSN\xspace}

% SmartMesh

\newcommand{\smip}                 {SmartMesh~IP\xspace}
\newcommand{\sm}                   {SmartMesh\xspace}
\newcommand{\dn}                   {Dust~Networks\xspace}
\newcommand{\ad}                   {Analog~Devices\xspace}
\newcommand{\hrdevice}             {{\tt HRDevice}\xspace}
\newcommand{\hrneighbors}          {{\tt HRNeighbors}\xspace}
\newcommand{\hrdiscovered}         {{\tt HRDiscovered}\xspace}
\newcommand{\hrextended}           {{\tt HRExtended}\xspace}
\newcommand{\pathcreate}           {{\tt path\_create}\xspace}
\newcommand{\pathdelete}           {{\tt path\_delete}\xspace}
\newcommand{\motecreate}           {{\tt mote\_create}\xspace}
\newcommand{\moteId}               {{\tt moteId}\xspace}

% Sol

\newcommand{\solmanager}           {{\tt solmanager}\xspace}
\newcommand{\solserver}            {{\tt solserver}\xspace}
\newcommand{\solsystem}            {SolSystem\xspace}

% misc

\newcommand{\ttA}                  {{\tt A}\xspace}
\newcommand{\ttB}                  {{\tt B}\xspace}
\newcommand{\ttC}                  {{\tt C}\xspace}
\newcommand{\ttD}                  {{\tt D}\xspace}
\newcommand{\ttE}                  {{\tt E}\xspace}
\newcommand{\ttF}                  {{\tt F}\xspace}
\newcommand{\ttG}                  {{\tt G}\xspace}
\newcommand{\ttH}                  {{\tt H}\xspace}

%----------------------------------------------------------------------------------------
%	LAYOUT SETTINGS
%----------------------------------------------------------------------------------------

% \setlength{\textwidth}{16cm}
% \setlength{\textheight}{25cm}
% \setlength{\oddsidemargin}{-0cm}
% \setlength{\topmargin}{-1cm}

%----------------------------------------------------------------------------------------
%	THESIS CONTENT 
%----------------------------------------------------------------------------------------

\begin{document}

\begin{center}
  {\large Thesis}
  
  \vspace*{0.2cm}
  of the
  
  \vspace*{0.2cm}

  {\Large École Doctorale Informatique, Télécommunications\\ et Électronique (Paris)}

  \vspace*{1cm}
  
  {\LARGE {\bf Deterministic Networking\\ for the Industrial IoT}}

  \vspace*{0.7cm}
  
  presented by
  \vspace*{0.4cm}
  
  {\Large Keoma BRUN-LAGUNA}
  \vspace*{1cm}
  
  A dissertation submitted in partial satisfaction of the\\
  requirements for the degree of
  \vspace*{0.3cm}

  {\Large Doctor of Philosophy}
  \vspace*{0.2cm}
  
  in
  \vspace*{0.2cm}
  
  {\Large Computer Science}
  \vspace*{0.2cm}
  
  at the
  \vspace*{0.2cm}

  {\Large Université Pierre et Marie Curie}
 
\end{center}

\vspace*{1.5cm} 
{\flushleft{}Presented in December 2018.\\[2ex]}

{\flushleft{Jury:}\\[1ex]}
{\flushleft{}\begin{tabular}{r@{\ }lll}
  & Fabrice {\sc Théoleyre} & CNRS (ICube), Strasbourg, France & Reviewer       \\
  & Sofie {\sc Pollin}    & KU Leuven, Leuven, Belgium         & Reviewer       \\
  & Lance {\sc Doherty}     & Analog Device, Cork, Ireland     & Examiner       \\
  & Marcelo {\sc Dias de Amorim} & LiP6, Paris, France         & Examiner       \\
  & Pascale {\sc Minet}     & Inria, Paris, France             & PhD Adviser    \\
  & Thomas {\sc Watteyne}   & Inria, Paris, France             & PhD co-Adviser \\
\end{tabular}}

% remove pages number
\pagenumbering{gobble}

% insert blank page
\newpage
\thispagestyle{empty}
\mbox{}

%==============================================================================
\chapter*{Abstract}

% IoT description

The Internet of Things (IoT) evolved from a connected toaster in 1990 to networks of hundreds of tiny devices used in industrial applications.
Those ``Things'' usually are tiny electronic devices able to measure a physical value (temperature, humidity, etc.) and/or to actuate on the physical world (pump, valve, etc).
Due to their cost and ease of deployment, battery-powered wireless IoT networks are rapidly being adopted.

% IIoT

The promise of wireless communication is to offer wire-like connectivity.
Major improvements have been made in that sense, but many challenges remain as industrial application have strong operational requirements.
This section of the IoT application is called Industrial IoT (IIoT).

% IIoT requirements and challenges

% -- reliability

The main IIoT requirement is reliability.
Every bit of information that is transmitted in the network must not be lost.
Current off-the-shelf solutions offer over 99.999\% reliability.
That is, for every 100k packets of information generated, less than one is lost.

% -- latency and lifetime

Then come latency and energy-efficiency requirements.
As devices are battery-powered, they need to consume as little as possible to be able to operate during years.
The next step for the IoT is to target time-critical applications.

% Contributions

Industrial IoT technologies are now adopted by companies over the world, and are now a proven solution.
Yet, challenges remain and some of the limits of the technologies are still not fully understood.
In this work we address TSCH-based Wireless Sensor Networks and study their latency and lifetime limits under \realworld conditions.

% -- Datasets Gathered (real-world and testbed)

We gathered \totalstats~network statistics \totalmeasurements~sensor measurements on \NUMDATASETS~datasets with a total of \NUMMOTEHOURS~mote hours in \realworld and testbeds deployments.
We assembled what we believed to be the largest dataset available to the networking community.

% -- TSCH Trade-offs

Based on those datasets and on insights we learned from deploying networks in \realworld conditions, we study the limits and trade-offs of TSCH-based Wireless Sensor Networks.
We provide methods and tools to estimate the network performances of such networks in various scenarios.
We believe we assembled the right tools for protocol designer to built deterministic networking to the Industrial IoT.
  
%==============================================================================

\tableofcontents

% insert blank page
\newpage
\thispagestyle{empty}
\mbox{}

%==============================================================================

% start page numbering
\pagenumbering{arabic}
\cleardoublepage
\setcounter{page}{1}

\chapter*{Acronyms}
\addcontentsline{toc}{chapter}{Acronyms}  
\label{chap:acronyms}

\begin{table}[h]
\begin{tabular*}{\linewidth}{l  r}
    ACK     & Acknowledgement (frame) \\
    ARHO    & American River Hydrologic Observatory \\
    ASN     & Absolute Slot Number \\
	COAP	& Constrained Application Protocol \\
    CSMA/CA & Carrier-Sense Multiple Access with Collision Avoidance (CSMA/CA) \\
    CTS     & Clear to Send \\
    CZO     & Critical Zone Observatory \\
    DetNet  & Deterministic Networking \\
    DODAG   & Destination-Oriented Directed Acyclic Graph \\
    ETSI    & European Telecommunications Standards Institute \\
    FDMA    & Frequency-Division Multiple Access \\
    FSPL	& Free Space Path Loss \\
    HART    & Highway Addressable Remote Transducer \\
    HR      & Health Reports \\
    HTTP    & HyperText Transfer Protocol \\
    IEEE    & Institute of Electrical and Electronics Engineers \\
    IETF    & Internet Engineering Task Force \\
    IIOT	& Industrial Internet of Things \\
    IOT		& Internet of Things \\
    IPC     & Industrial Process Control \\
    IPv6    & Internet Protocol version 6 \\
    JSON    & JavaScript Object Notation \\
    KPI		& Key Performance Indicators \\
    LLN     & Low-Power and Lossy Network \\
    LOS     & Line Of Sight \\
\end{tabular*}
\end{table}
\begin{table}[h]
\begin{tabular*}{\linewidth}{l  r}
    MTU     & Maximum Transmission Unit \\
    M2M     & Machine-to-Machine \\
    OQPSK   & Offset quadrature phase-shift keying \\
    OSI     & Open Systems Interconnection \\
    PDR     & Packet Delivery Ratio \\
    PLC     & Power Line Carrier \\
    ppm     & parts per million \\
    PRR     & Packet Reception Rate \\
    QoS     & Quality of Service \\
    RFC     & Request For Comments \\
    RFID    & Radio Frequency Identifiers \\
	RPL    	& Routing Protocol for Low-Power and Lossy Networks \\
    RTS     & Request to Send \\
    SDO     & Standards Developing Organizations \\
    SNR     & Signal-to-Noise Ratio \\
    SOL 	& Sensor Object Library \\
    TASA    & Traffic Aware Scheduling Algorithm \\
    TDM     & Time Division Multiplexing \\
    TDMA    & Time Division Multiple Access \\
    TLS     & Transport Layer Security \\
	TSCH 	& \tsch \\
    TSMP 	& Time Synchronized Mesh Protocol \\
    UDP	    & User Datagram Protocol \\
    WSN  	& Wireless Sensor Networks \\
    6LoWPAN & IPv6 over Low-Power Wireless Personal Area Networks \\
    6P      & 6top Protocol \\
    6TiSCH  & IPv6 over TSCH \\
    6top    & 6TiSCH Operation Sublayer \\
\end{tabular*}
\end{table}

%==============================================================================
\chapter*{Glossary}
\addcontentsline{toc}{chapter}{Glossary}  
\label{chap:glossary}

\hspace{\parindent}\textbf{Mercator} is a system to collect large wireless connectivity datasets that are dense in time, space and frequency.
This system allows us to understand the quality of wireless connectivity, and its variation over time and frequency.

\textbf{\solsystem} is a set of tools we developed and used in our real-world deployment.
\solsystem includes
    \textit{(i)} software that runs next to the deployment gateway.
        It backs up the data locally, parses it and forwards it to the API.
    \textit{(ii)} software that runs in a datacenter.
        This software parses, stores, analyzes, and displays the data.

\textbf{TSCH}: \tsch (TSCH) is a medium access control method for shared medium networks.
TSCH is designed for applications that require reliability and ultra long battery life.

\textbf{\6}: \6 is a working group at the IETF which is standardizing how to combine IEEE802.15.4 \tsch (TSCH) with IPv6.
In this work, I also use the terms \textit{\6 stack} and \textit{\6 network} to refer to the stack of networking protocols defined by the working group, and a network using that \6 stack, respectively.

%==============================================================================
\chapter*{Acknowledgements}
\addcontentsline{toc}{chapter}{Acknowledgements}  
\label{chap:acknowledgements}

\begin{figure}[htp]
\centering
\includegraphics[width=0.25\linewidth]{logo_upmc.png}\hspace{1cm}
\includegraphics[width=0.25\linewidth]{logo_edite.png}\hspace{1cm}
\includegraphics[width=0.25\linewidth]{logo_inria.png}
\end{figure}

The thesis work was made possible thanks to National Institute of Research in Computing and Automation (Inria), and Doctoral School of the University Pierre et Marie Curie (UPMC) in Paris.
I would like to send my deepest appreciation to my advisor Thomas Watteyne, for the continuous support, patience, and encouragement he gave through my work.
Without him I would not had the chance to work with such competent team across the world, and to experience the cutting edge of technology in such great conditions.
He always gave me the motivation and advice I needed.
I deeply thank my adviser Pascale Minet for her patience, and sharp advice through my thesis.
I thank Professor Diego Dujovne from University of Diego Portales in Santiago, Chile, for hosting me in his laboratory as well as for his support and collaboration in the \peach project, together with Gustavo Mercado, Ana Laura Diedrichs and Carlos Taffernaberry from the National University of Technology of Mendoza, Argentina.
I thank Professor Steven D. Glaser from UC Berkeley for hosting me in his laboratory and allowing me to participate in \realworld deployments in the Sierra Nevada together with Dr.~Carlos Oroza, Sami Malek and Dr.~Zeshi Zhang.
I thank Mrs. Sofie Pollin, assistant professor at KU Leuven, Belgium and Mr. Fabrice Théoleyre, CNRS Researcher at the University of Strasbourg, France, for having accepted to review my thesis.
I thank Lance Doherty, IoT Systems Architect at \ad, and Mr.~Marcelo Dias de Amorim, CNRS Researcher at the University Pierre et Marie Curie (Paris 6) for serving as examiners of my thesis.
I thank Jonathan Muñoz, Dr.~Tengfei Chang, Yasuyuki Tanaka, Dr.~Mališa Vučinić, Dr.~Ziran Zhang, and  Dr.~Rémy Léone for all their advises and support. 
I thank the undergraduate students I have had the privilege of working with for their help, and especially, Marcelo Ferreira, Felipe Moran and Fabian Rincon for their true engineering skills.

%==============================================================================
\input{chapters/01_introduction}
\input{chapters/02_state_of_the_art}
\input{chapters/03_methodology}
\input{chapters/04_realworld}
\input{chapters/05_characterizing}
\input{chapters/06_tsch-limits-and-tradeoffs}
\input{chapters/98_conclusion}
\input{chapters/99_publications}

%==============================================================================

\printbibliography[
    heading=bibintoc,
]

%==============================================================================

% insert blank page
\newpage
\textcolor{white}{POUYACHAKA}
\thispagestyle{empty}
\mbox{}

\end{document}