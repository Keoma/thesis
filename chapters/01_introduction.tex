\chapter{Introduction}
\label{cha:introduction}

\chapeau{
    In this chapter, we introduce the notions about the Internet of Things (IoT) that are used throughout this thesis.
    We explain the reasons that led us to carry out this work, and we present our contributions.
}

%==============================================================================
\section{Preliminaries}
\label{sec:preliminaries}

%------------------------------------------------------------------------------
\subsection{The Internet of Things}
\label{sub:the_internet_of_things}

% intro on the IoT 

The Internet of Things (IoT) has so many definitions that the first thing I want to do is to rename it to fit my understanding.
The broadest definition I have in mind is ``tiny computers connected to the Internet''.
The term \textit{Internet of Things} was first coined by Kevin Ashton in 1999 to describe the communication between a Radio Frequency Identifiers (RFID) device and the Internet. 
The term was rapidly adopted by governments and leading Information Technology (IT) companies to define a technology concept that would bring sustainability and economic growth.
The way those ``tiny computers'' are connected to the Internet and what protocols they use varies a lot.
The first ``connected Thing'' was a toaster, built in 1990 by John Romkey and Simon Hackett.
The toaster was using TCP/IP and SNMP and was mains-powered.
We now see devices the size of a grain of rice, battery powered, and that can communicate wirelessly~\cite{mesri16design}.

% introduction to IIoT

In this document, I use the term \textit{Industrial Internet of Things}~(IIoT) to describe a subset of the IoT targeted towards reliability.
The IoT brings many advantages such as ease of deployment and cost reduction, and its adoption by the Industry is increasing.
The IoT is now a major component of the Industry~4.0, a vision where machines are capable of sensing their physical environment, taking decentralized decisions and collaborate with other systems or users.

%------------------------------------------------------------------------------
\subsection{Wireless Sensor Networks}
\label{sub:wireless_sensor_networks}

% description

The term ``IoT'' encompasses many technologies.
In this thesis, I focus on a subpart of the IoT that is the Wireless Sensor Networks (WSNs).
Wireless Sensor Networks are networks of small electronic devices -- called nodes -- that communicate wirelessly using radio waves\footnote{Electromagnetic waves on the radio spectrum (3~Hz to 3~THz)}.
In Wireless Sensor Networks, nodes are organized around one (or multiple) collecting device(s) -- called \textit{sink} or \textit{gateway}.
The networks are usually ``multi-hop'', that is, nodes serve as data relays to forward data to the sink.

\begin{figure}[h]
    \centering
    \includegraphics[width=\columnwidth]{wsn}
    \caption{An example of multi-hop Wireless Sensor Network topology.}
    \label{fig:wsn}
\end{figure}

% the WSN promise

Wireless Sensor Networks (WSNs) need to offer at least the same capabilities as their wired counterpart.
They need to be reliable, scalable, and durable, but also be easy to deploy, cost-effective and flexible.
Most of those goals are achieved today.
Solutions exist offering wire-like reliability with networks of hundreds of nodes, able to operate for years, deployable in only a few hours at a very low cost.
The following chapter gives the reader an idea of how those requirements are achieved.

% Battery Constraint

\sloppy The major difference between traditional wireless networks such as IEEE802.11~(WiFi) or IEEE802.15.1~(Bluetooth) is that WSN nodes need to run over years on battery.
This means that WSN nodes are heavily optimized to consume as little as possible.
The main energy consumption source of those devices is the radio chip.
Reducing the use of this chip reduces the node's energy consumption, yet
it makes communication between nodes more complex.

% Wireless Impairment

Wireless is unreliable by nature.
Unlike wired connection, wireless suffers from external interference, and is affected by different types of fading. 
This does not mean that there is no way of building a reliable service on top of it.
There are multiple techniques available to mitigate the radio medium uncertainty; we survey those in Chapter~\ref{cha:state_of_the_art}.
Wireless constraints are present under different forms, we now present them in more details.

%------------------------------------------------------------------------------
\subsection{The Wireless Impairment}
\label{sub:wireless_constraints}

% intro

We divide the wireless constraints in two parts,
   \textbf{external interference} and
   \textbf{fading}.

% External interference

External interference occurs when multiple devices transmit at the same time on the same frequency\footnote{~This is not true for all technologies. For instance when using modulations such as OFDM.}.
In that case, the radio waves might collide, resulting in data loss.
Such external interference is very present in the environments we study in this work as the technology we use communicates on an Industrial, Scientific and Medical (ISM) radio band.
ISM bands are portions of the radio spectrum that are not subject to license.
ISM bands are subject to rules (e.g.~transmission power, data-rate) but do not require users to pay a usage fee.
Regulations and bandwidth only varies a little across continents making their adoption by communication technologies easier.
As a consequence, the radio communication contention is high in those bands, and particular care needs to be taken when designing protocols.
We will see how this is achieved in Chapter~\ref{cha:state_of_the_art}.

% Free Space Path-Loss

The other wireless constraint is fading.
Fading is the attenuation or variation of a radio signal.
The first type of fading we can think about is called \textit{Free Space Path-Loss} (FSPL).
Free Space Path-Loss is the attenuation of a signal over distance.
The radio energy dissipates making the amount of energy received lower than the amount of energy emitted.

% Shadowing

The second type of fading I want to present is called \textit{Shadowing} or \textit{Shadow Fading}.
Shadowing is also an attenuation but this time due to obstacles.
When the radio wave hits an obstacle (e.g.~wall, tree, water), the amount of energy that passes through is inferior to the amount energy before the obstacle.

% Multipath

The last type of fading I want to mention is named \textit{Multipath Fading} and is less intuitive.
Multipath Fading can be seen as self interference. 
When a signal is transmitted using radio waves, a receiver hears the signal that traveled following the direct line-of-sight (LOS), but also the echoes of the signal that bounced on nearby objects.
Those ``echoes'' can be constructive or destructive.
In the latter case, a fading occurs and the signal can become unexploitable.
This phenomenon is called Multipath Fading.
Multipath Fading is not related to distance, as shown in~\cite{watteyne09reliability, watteyne10mitigating}, Multipath Fading can occur over very small distances.
Multipath Fading increases when the number of reflecting objects increases.
Fortunately, the effects of Multipath Fading differs depending on the signal frequency.
We will see how this is taken advantage of in Chapter~\ref{cha:state_of_the_art}.

\begin{figure}
    \centering
    \begin{subfigure}[t]{0.20\textwidth}
        \includegraphics[width=\columnwidth]{wireless_constraints_external_interferences}
        \label{fig:wireless_constraints_external_interferences}
        \caption{External Interferences}
    \end{subfigure}
    \quad
	\begin{subfigure}[t]{0.20\textwidth}
        \includegraphics[width=\columnwidth]{wireless_constraints_distance}
        \label{fig:wireless_constraints_distance}
        \caption{Free Space Path Loss}
	\end{subfigure}
    \quad
	\begin{subfigure}[t]{0.20\textwidth}
        \includegraphics[width=\columnwidth]{wireless_constraints_obstacles}
        \label{fig:wireless_constraints_obstacles}
        \caption{Shadowing}
	\end{subfigure}
    \quad
	\begin{subfigure}[t]{0.20\textwidth}
        \includegraphics[width=\columnwidth]{wireless_constraints_multipath}
        \label{fig:wireless_constraints_multipath}
        \caption{Multipath}
	\end{subfigure}
    \caption{The different Wireless Constraints we are faced with in WSN.}
    \label{fig:wireless_constraints}
\end{figure}

% conclusion

Fig.~\ref{fig:wireless_constraints} illustrates the main wireless constraints we are faced with when using WSNs.
This list of constraints is not exhaustive, but rather gives an overview of the main challenges that need to be taken into account when designing WSN protocols.

%------------------------------------------------------------------------------
\subsection{Applications and Market Opportunities}
\label{sec:applications_and_market_opportunities}

% Application examples

Although computer-based monitoring has existed for years, the ability to be deployed densely at a very low cost opens a wide scope of applications to WSN and the IoT:
    Environmental Monitoring,
    Precision Agriculture,
    Building Automation,
    Factory Monitoring
    and many more.
Dense monitoring information brings new insights and opens up a new era of competitiveness and growth.
This list is not exhaustive and the details of each application type may not be true in every situation.
This list is only presented here to give an overview of the potential IoT applications for IoT.
Table~\ref{tab:iot_application_requirements} summarizes those types of applications and their requirements.

\textbf{Home Automation}: In those applications, the system can for instance control lighting, HVAC, or appliances in order to optimize a house consumption or comfort.
To do so, the system is equipped with sensors and actuators that can communicate with each others forming networks.
Those networks require low latency (e.g.~switching light, changing music volume) and tolerate loss.
Many IoT devices in home automation are powered with wires.
Home Automation networks are usually in range of a gateway, meaning that the topology is a star.

\textbf{Smart Metering}: The goal of those applications is to periodically record consumption (e.g.~electricity, gas) for monitoring and billing.
Those applications require very low data rate and payload size (a few tenth of bytes per hours or per day) and do not require low latencies.
Devices can be powered by batteries or with wires.
They usually are located in urban environment at a range of a gateway (could be kilometers away).

\textbf{Environmental Monitoring}: The goal of these applications is to characterize an environment in order to asses its health or understand its behavior.
Unlike the other types of application, actuation is rarely involved.
Devices are located far away from each others and sometimes not in range of the gateway.
Unlike previously presented types of application, they might need to use other devices (relays) to forward their data to the gateway.
They are usually located in harsh environments (subject to animals attack, tree falling, wind, rain, \dots) in remote location.
Replacing batteries or using wires is not an option in those use cases, those networks need to work during years on batteries.
There is no strong requirement in reliability (e.g.~a humidity data point loss is tolerated).
As networks are located outdoor, they rarely suffer from multipath effects or external interferences. 

\textbf{Industrial IoT (Industrial Monitoring and Automation)}: In those applications, the goal is to provide industrial machines the ability to sense their environment and react according to it.
Those applications have very strong reliability requirements.
When data is lost, it can directly impact the automation process (supply chain interruption) and result in loss of production.
The devices are usually densely located, and surrounded by metal objects, thus multipath and external interferences are common in those environments.

\begin{table}[h]
  \begin{adjustbox}{width=1.4\textwidth,center=\textwidth}
  \begin{tabular}{|c|c|c|c|c|c|c|}
    \hline
	Application 	& Reliability & Latency & Topology & Links Dist. & Radio Env. & Power \\
    \hline
    Home Automation & Low 		  & High 	& Star 	& $<100m$ & Multipath \& Ext. Int. & Battery or Wires \\
    Smart Metering 	& Low 		  & Low 	& Star	& $>500m$ & Multipath \& Ext. Int. & Battery or Wires \\
    Env. Monitoring & Medium	  & Low		& Mesh 	& $>500m$ & No Multipath, no Ext. Int. & Battery\\
    Industrial IoT  & High 		  & Low		& Mesh 	& $<50m$  & Multipath \& Ext. Int. & Battery \\
    \hline 
  \end{tabular}
  \end{adjustbox}
  \caption{A summary of the IoT applications and their requirements.}
  \label{tab:iot_application_requirements}
\end{table}

% Turning point

We are now at a turning point for WSN and Industrial IoT.
In the last ten years, the lack of a fully standardized network stack led companies to implement their own proprietary solutions.
As a result, no interoperability was possible between the different IIoT companies, slowing down market adoption.
This is over as Standards Developing Organizations (SDOs) are now proposing fully standardized, IPv6-ready, network stacks that provide the requirements the IIoT needs.

% Standardization

SDOs are professional associations that create and maintain normative technical documents (standards) that describe how network protocols should work.
The standard elaboration process is open to the community and publicly available without any discriminatory conditions.
This profits to vendors as they can penetrate market more easily -- as those standard are usually world-wide adopted --, but also customers as they can access the technology without being tied to one company.
The main SDOs in IIoT are
    the IEEE (link and physical layers),
    ETSI (complete machine-to-machine solutions),
    ISA (regulation for control systems) and
    the IETF (routing and network layers).

% Future

Thousand of proprietary IIoT networks are deployed in the world today.
The interoperability between those networks and devices can widen the application scope even more.
While I can only foresee application-layer interoperability in industries, the coexistence of solutions in city-scale automation seems logical to me.

% Challenges

There is still work to be done to fully understand the capacity of IIoT networks.
For instance, actuation and control loops is still at its infancy, and applications that provide industry-grade guarantees are in early development.
The goal of my thesis is to reduce this gap, and trying to understand how much guarantee those networks can provide.

% quick summary

In this section, I defined what I mean by IoT and IIoT, what WSNs are and what main challenges to keep in mind when using WSNs.
I show that applications can be very diverse and that the IoT market is still in expansion.

%==============================================================================
\section{Contributions}
\label{sec:contributions}

My contribution is threefold:

\begin{itemize}
    \item I participated in the deployment of four \realworld WSN deployments and gathered more than \totalstats over two years, creating what we believe to be the largest \realworld WSN dataset available.
    \item I analyzed, benchmarked and compared those datasets with IoT testbeds and defined a set of requirements that need to be taken into account when designing IoT protocols.
    \item I showed that determinism in Industrial IoT networks can be achieved and built a tool to estimate the performances of Industrial IoT networks.
\end{itemize}

%------------------------------------------------------------------------------
\subsection{Network Deployments and Data Collection}

During the first year of my thesis I actively participated in the deployment of four real-world WSN deployments:

\begin{itemize}
    \item \textbf{\peach:} A frost prediction system for peach orchards in Mendoza (Argentina).
        We deployed \peachNumNodes nodes to measure temperature and humidity at different locations in the orchard.
    \item \textbf{\snowhow:} A snow-pack monitoring system in the Sierra Nevada (California).
        We deployed \snowhowNumNodes nodes to measure snow level, solar radiation and more in the mountain.
    \item \textbf{\evalab:} A Smart Building monitoring system in Paris (France).
        We deployed \evalabNumNodes nodes to measure temperature in an office building.
    \item \textbf{\smartmarina:} A metering and management system for marinas in Agde (France).
        We deployed \smartmarinaNumNodes nodes to measure electricity consumption in the marina.
\end{itemize}

Together, those deployments generated more than \totalmeasurements sensor measurements and \totalstats network statistics.
I built the rest of my work on top of those datasets.

%------------------------------------------------------------------------------
\subsection{Data Analysis and Comparisons}

% real-world analysis

I first looked into the real-world network statistics and started my first network characterization and benchmarking study.
I produced plots and results that indicate how well a network behaves, and found that in some cases links can be considered symmetric and that network churn (node parent change) could be only a few per day.

% testbed analysis and comparison

Then I compared real-world and testbed results.
Using the \iotlab, a group of open testbeds in France, I collected dense radio connectivity traces (i.e.~radio link qualities over time).
Comparing those traces results with the ones collected in real-world deployments, I identified weaknesses of testbeds and provided a list of Key Performance Indicators (KPI) to watch for when designing IoT protocols.

%------------------------------------------------------------------------------
\subsection{Determinism in IIoT}

As we said earlier, the radio medium is unreliable by nature.
Given such statement is it possible to identify behaviors that are predictable in WSN ?
By using \tbs to replay real-world and testbed results, I identified the limits of WSN regarding latency and network lifetime.
I showed that when a set of conditions are met, we can predict the performances of a WSN.
I designed and built a tool to help WSN protocol designers having a better idea of what to expect of their networks.

%==============================================================================
\section{Thesis outline}
\label{sec:thesis_outline}

\hspace{\parindent}\textbf{The second chapter} presents the state of the art.
It starts from an overview of the existing WSN protocols before going into the details of the lastly adopted networking stack for the Industrial IoT.

\textbf{The third chapter} describes the methodology and assumptions we decided to take in this work.
Unlike the mostly adopted approach that consists in reading the maximum number of papers and articles of the domain literature, we decided to start by manipulating networks in real-world conditions and understanding their real limits before proposing any improvement.
I believe that this approach strengthened my opinions when taking improvement decisions and allowed me to easily spot unrealistic assumptions.

\textbf{The fourth chapter} details our real-world deployments.
For each deployment we explain
    its application and challenges,
    the hardware and software choices we made,
    how many nodes were deployed,
    what data we collected.
We also give a first idea of the performance of the network and list the lessons learned.

\textbf{The fifth chapter} presents the experiments we ran in testbeds and the results we obtained by comparing testbeds with \realworld deployments.
In particular, I talk about the Mercator project, a dense radio connectivity data collection system.
We identified the network Key Performance Indicators (KPI) and behaviors one must observe in order to validate an IoT protocol.

\textbf{The seventh chapter} focuses on studying the limits and trade-offs of WSN scheduling regarding latency and network lifetime.
I explain why we think that \tbs is the right tool for such study and compare real-world results with theoretical limits.
I finish by presenting the \6 Performance Estimator, a tool I built to estimate the performances of IIoT networks.

