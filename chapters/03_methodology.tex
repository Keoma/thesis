\chapter{Methodology and Assumptions}
\label{cha:methodology}

\chapeau{
    In this chapter, we describe the methodology we follow to conduct our work.
    We start by deploying real-world IoT solutions and gathered what we believe to be one of the largest real-world IoT datasets available.
    Based on this ground work we propose improvements and recommendations for the IIoT.
}

Our research methodology is not the usual one as it does not follow the typical order: proposition-measurements-validation.
We believe that to be able to fully understand a system and its limits, we need to ``get our hands dirty''.
We thus started by the measurements, gathering data and insights from real-world and testbed deployments.
This allows us to ground or work in realistic assumptions, and produce solid results based on extensive datasets.
From this ground work, we identify the limits and trade-offs of the networks we study, and propose enhancements.
We then test and validate our proposals through simulation and experimentation. 

%==============================================================================
\section{Real-World Deployments}
\label{sec:real-wold_deployments}
 
\textbox{
    What is the state-of-the-art in IIoT and what are the \textit{real} challenges the IIoT is faced with?
}

% intro (what do we mean by real world)

What do we mean by \realworld networks?
We consider a real-world network as a network that serves a real application, that is, an application whose purpose is not (or not only) to collect network statistics or test a system.
Note that the network environment is not mentioned in that definition.
There is neither a ``right'' nor a ``wrong'' network environment, as low-power wireless applications are very diverse. 

% simulation/emulation

While simulation and emulation can help proving theoretical protocol designs, they quickly face limits.
There is a trade-off between realism and complexity:
    fully simulating the radio environment would lead to overly complex computation,
    but using simple models often does not represent reality.
A common model type is the \textit{Path-Loss} model that describes the signal attenuation between a transmitter and a receiver as a function of the propagation distance, on top of which other parameters can be added such as carrier frequency, antenna positions or terrain profile.
Models such as the \textit{Path-Loss} model rarely take into account multi-path fading or the variation of the radio environment over time.

% testbeds

Deploying hardware is more costly and creates systems that represent a single environment.
A smart building deployment will always be an indoor deployment and the location of the devices will not change to adapt to a given application setup and then to another.
Results obtained using testbeds are however more trustworthy than simulation as they are proven to work on real physical hardware (e.g.~processing and memory delays and limits) and environments (radio propagation and interferences). 
Many testbeds are now being deployed and used to validate protocols.
Although testbeds are more realistic, we show in Chapter~\ref{cha:characterizing} that they might not show the worst case characteristics of a \realworld network and that results obtained from testbeds might not be sufficient to fully validate a protocol.

% realworld

Deploying a network that serves a purpose other than testing protocols (i.e.~a \realworld network) helps ensuring a high level of realism, from the position of the devices, to the current consumed, the radio environment and the data traffic.
We thus decided to start with \realworld deployment to better understand the WSN and their challenges.
I actively participated in the deployment of four real-world WSN deployments: 
\begin{itemize}
    \item \textbf{\peach}:
        A frost prediction system for peach orchards in Mendoza (Argentina).
        We deployed \peachNumNodes devices on a \peachArea area to measure temperature and humidity at different locations in the orchard.
    \item \textbf{\snowhow}:
        A snow-pack monitoring system in the Sierra Nevada (California).
        We deployed \snowhowNumNodes devices in a \snowhowArea area to measure snow level, solar radiation and other environmental data in the mountain.
    \item \textbf{\evalab}:
        A Smart Building monitoring system in Paris (France).
        We deployed \evalabNumNodes devices in a \evalabArea area to measure temperature in an office building.
    \item \textbf{\smartmarina:}
        A metering and management system for marinas in Agde (France).
        We deployed \smartmarinaNumNodes devices in a \smartmarinaArea area to measure the presence and electricity consumption of the boats in the marina.
\end{itemize}

% solsystem

As no end-to-end solution existed that suited our needs to format, transfer and store our data, we created \solsystem.
We took existing tools and assembled them, together with custom Python scripts, to create a system that is able to:

\begin{itemize}
    \item \textbf{format data}:
        as some deployments are connected to the Internet using a satellite connection, we needed to highly compress the data produced by the network.
        We designed the concepts of ``SOL Objects'', a way of representing data in a compressed way, to which Object Security can be applied~\cite{vucinic15oscar}.
    \item \textbf{transfer}:
        as we wanted to centralize the data of each deployment for easy treatment, we created a system to transfer the data from the gateway on the deployment site to a remote server.
    \item \textbf{store}:
        to make sure we do not loose any information, data is compressed and stored in both the gateway and the remote server.
    \item \textbf{display}:
        we use existing tools to visualize to visualize the data in real-time.
        This allowed us to have a clear view of what is happening in the field, in real-time.
\end{itemize}

% conclusion and lessons learned

These deployments gave me two things:
    \realworld insights, and
    a large amount of data.
We verified intuitive assumptions we had about \realworld deployments such as
    network density,
    the relation between RSSI and distance, and
    overall network performances (reliability and lifetime).
We went further and obtained not-so-intuitive results about link asymmetry and network churn.

We describe the deployments and their results in Chapter~\ref{cha:realworld}.

%==============================================================================
\section{Characterizing Networks}
\label{sec:characterizing_networks}
 
\textbox{
    How to characterize the radio environment a network is deployed in, and what are the key performance indicators to look at?
}
 
 % intro
 
After learning about \realworld networks, I wanted to get more insights from testbed deployments.
As I started to understand the challenges of WSNs, the need for denser connectivity data increased.
One key advantage of using testbeds is that it allows out-of-band communication.
That is, devices are usually powered by cables and accessible using wired communication (e.g.~serial communication).
This allows users to
    program the devices by loading firmware, \
    trigger commands to interact with the running system,
    or debug at run time by inserting breakpoints or observing logs.
Out-of-band communication enables the collection of dense network statistics.

In \realworld networks, we collect sensor data and network statistics.
Network statistics are useful for the network to operate well, but should not reduce the resources for the sensor data to be carried over the network.
Network statistics are therefore usually sent at a low data rate.
In our deployments, network statistics are generated by each device every 15~min, and contain averages of statistics over the last 15~min.
To better understand the behavior of TSCH, I needed a denser statistics, i.e. more data.

% testbed vs real-world

Both testbed and \realworld networks suffer from the same limitation: they only represent one scenario.
In order to be properly validated, a system needs to be tested in all situations it is to be used in, or at least in the worst case scenarios.
Testbeds are often considered realistic, but rarely represent a worst-case scenario.
After observing testbeds that are part of the \iotlab, we found that they present only low external interferences from other technology (e.g.~WiFi) and rarely show changes in radio connectivity.
A protocol should not be validated by using only results obtained in such radio conditions unless the protocol is designed only for such conditions.
This emphasizes the need for new ways of validating wireless protocols and increase testbed diversity.

% short applications and deployments description

To get those values I use the \iotlab, 6 large testbeds deployed throughout France, and designed for network design and testing.
To collect connectivity data that is dense in time, space, and frequency, I modified and used a system called \mercator.
I describe each experiment and its results in Chapter~\ref{cha:characterizing}.
Using \mercator, I collected radio connectivity traces that allow me to know the quality of the radio links between two devices in every available radio channels, and this, several times per minute.

% formatting traces

In order to compare the datasets obtained from \solsystem and \mercator, I designed K7, a simple file format (extended CSV) that allows to present both types of data in a uniform way.

% the 3-phenomena

By extracting information from both testbed and \realworld results, we were able to identify key parameters to look at when characterizing a network environment.
We identified 3~phenomena that are the most common in real-world deployments, and which have a deep impact on connectivity:
    external interference,
    multi-path fading,
    dynamics in the environment.
We consider those as the worst-case radio environments, and claim that a WSN protocol should be tested under such conditions (at least) in order to be validated.

% conclusion

Analyzing those radio connectivity traces and network statistics further improved my understanding of how the radio environment behaves.

%==============================================================================
\section{TSCH Limits and Trade-offs}
\label{sec:tsch_limits_and_tradeoffs}

\textbox{
    Given a required end-to-end reliability, what is the minimum possible latency when using TSCH?
    How many years does the network loose if I want to reduce the end-to-end upstream latency by \textit{X}~seconds ?
}

The Industrial IoT technologies are now adopted by many companies over the world and are now a proven solution.
Yet challenges remain and some of the limits of such technologies are still not fully understood.
The use of TSCH-based networks to support time-critical applications is still at its early stages.
More specifically, the trade-offs between latency and energy consumption (lifetime) are still not well understood.
In this work, we try to fill that gap and study the limits of TSCH-based networks in term of:

\begin{enumerate}
    \item \textbf{end-to-end upstream latency}.
        The time for a message generated by a sensor node to be delivered to the gateway.
    \item \textbf{energy consumption}.
        How much energy is required to achieve a given latency limit.
\end{enumerate}

To do so, we simulate networks on top of the connectivity traces obtained from both \realworld and testbeds experiments.
The connectivity traces replaces the (typically used) radio propagation models and provide results based on \realworld radio environment. 
We recommend trace-based simulation as a tool for protocol performance evaluation.

%==============================================================================
\section{Summary}

In this chapter, we detail our methodology and assumptions.
We explain how we go from exploring \realworld deployments, to studying the performances limits and trade-offs of TSCH-based networks through trace-based simulation.
We gather network statistics from \realworld deployments from which we extract a set of conditions that we believe should be present to have representative results.
We then study the limits and trade-offs of TSCH-based network in term of
    reliability,
    latency and
    energy consumption.
By following a non-traditional approach, we believe we have identified realistic challenges and assumptions to propose results that are both representative of a real deployment, and immediately useful.
