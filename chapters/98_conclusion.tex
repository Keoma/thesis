\chapter{Conclusion and Perspectives}
\label{cha:conclusion}

% context

Currently, commercial IIoT products offer wire-like end-to-end reliability ($>$99.999\%) together with very long network lifetime ($>$10~years).
While those solutions are not fully standardized yet, they will soon be, and we consider E2E reliability issues as a solved problem.
In the meantime, network solutions that address time-critical applications are at their early development stage, and their latency performance bounds are still not fully understood.
This thesis contributes to filling this gap and understand the performance limits and trade-offs of TSCH-based networks in terms of e2e latency and network lifetime.

%------------------------------------------------------------------------------
\section{Contributions}

% we deployed real-world networks and evaluated their perfs

To start with a clear understanding of the current IIoT challenges, we deployed 4~\realworld low-power wireless sensor networks:

\begin{itemize}
    \item \textbf{\peach}:
        A frost prediction system for peach orchards in Mendoza (Argentina).
        We deployed \peachNumNodes devices on a \peachArea area to measure temperature and humidity at different locations in the orchard.
    \item \textbf{\snowhow}:
        A snow-pack monitoring system in the Sierra Nevada (California).
        We deployed \snowhowNumNodes devices in a \snowhowArea area to measure snow level, solar radiation and other environmental data in the mountain.
    \item \textbf{\evalab}:
        A Smart Building monitoring system in Paris (France).
        We deployed \evalabNumNodes devices in a \evalabArea area to measure temperature in an office building.
    \item \textbf{\smartmarina:}
        A metering and management system for marinas in Agde (France).
        We deployed \smartmarinaNumNodes devices in a \smartmarinaArea area to measure the presence and electricity consumption of the boats in the marina.
\end{itemize}

We collected a total of \totalstats network statistics and \totalmeasurements sensor data and made those publicly accessible.
We believe that this is the largest \realworld low-power wireless dataset available to the research community.
We analyzed those datasets and found not-so-intuitive results, such as the fact that a very low number of routing changes (less than 5 per day) are actually happening in \realworld deployments.

% we collected dense connectivity traces

We collected radio connectivity traces on different testbeds to have data that is dense in time, space and frequency.
We compared those radio connectivity traces with results obtained from the \realworld deployments, and identified key metrics and behaviors that must be taken into account when developing a protocol for the IIoT.
We further proposed a methodology to ensure that a protocol that performs well on a testbed also does so when moving beyond testbeds.
More specifically, we looked at
    External Interferences,
    Multipath Fading, and
    their variations over time.

% we replayed those traces in a simulator and quantified perf tradeoffs of TSCH-based networks

We identified the theoretical limits and trade-off points of the Time Slotted Channel Hopping (TSCH) mode of the IEEE~802.15.4 MAC layer.
We focused on
    E2E upstream reliability,
    E2E upstream latency, and
    network lifetime.
We tested those theoretical limits against \realworld performances by ``replaying'' connectivity traces and topologies in the \6 simulator.
We show that we are able to quantify the E2E upstream latency bounds in 99.999\% of the network packets.
We also propose a method to estimate the trade-off between network lifetime and E2E upstream latency.

% we provide a tool (6 perf est) for protocol designers to evaluate the perfs

Finally, we present the \6 Performance Estimator, a tool for low-power wireless protocol designers to estimate the performance of a network, given a set of inputs (e.g.~network topology, connectivity traces, traffic).

%------------------------------------------------------------------------------
\section{Perspectives}

% - - - - - - - - - - - - - - - - -
\subsubsection{Designing Protocols for the IIoT}

The available of open testbed platforms to the research community increased the impression of realism in the networking protocol validation process.
Yet, a solution validated on a testbed might not work well when deployed in \realworld conditions.
We still lack a generic way of characterizing networks experimentation.
The IoTBench initiative proposes an architecture to characterize a network experiment considering its
    \textit{inputs},
    \textit{outputs}, and
    \textit{observed} metrics.
I believe adopting a common benchmarking architecture is the right approach to measure and compare network experiments in a standard way, in order to produce results that one can rely upon.

% - - - - - - - - - - - - - - - - -
\subsubsection{Trace-based Simulation}

Simulation is a great tool for rapid evaluation and performance estimation of networking protocols.
In wireless networking simulators, the component to simulate which is most tricky is often the radio propagation.
Using models that are too simple often leads to unrealistic results;
    models that are too complex often leads to high computation and processing time.
I believe \tbs should be used more, as it offers a good trade-off between complexity and realism.

% - - - - - - - - - - - - - - - - -
\subsubsection{\6 Performance Estimator}

% add feature to identify bottleneck and possible improvements (distance to optimal)

The \6 Performance Estimator is a tool to help protocol designers estimate the performance of their protocols.
To go further than just providing performance estimation, this tool should be extended to identify the performance bottlenecks and protocols weak points.
We could consider using Machine-Learning techniques to identify performance bottlenecks that are not straightforward.

% a few words about its use in the standardization process

Both the \6 Simulator and \6 Performance Estimator are made to test the capabilities and performances of the \6 networking stack.
We envision those tools to be part of the standardization process and that they should be systematically used before validating any protocol in \6.

% - - - - - - - - - - - - - - - - -
\subsubsection{Node's Mobility}

So far we only considered networks where nodes are static.
Our work could be extended to encompass networks with mobile nodes.
As reserving resources along a path takes times and energy, I do not believe that networks where every node is mobile can offer reliability or latency guarantees.
However, I think that hybrid networks can.
By having a fixed backbone the network can allocate extra resources to carry traffic of potential leaf mobile nodes.
Thus, the mobile nodes only have to allocate resource with their parents, the rest of the path to the root being already reserved.
Such scheduling policy needs of course to be tested and validated.

% - - - - - - - - - - - - - - - - -
\subsubsection{Downward Traffic \& Actuation}

In this thesis, I only studied upward traffic for convergecast applications.
This type of traffic encompasses periodic monitoring applications and event-driven data collecting applications.
We showed that TSCH-based networks can provide predictable performances, and can be used in time-critical applications.
However, we did not study the performance of such networks when using downstream traffic.
Providing latency performances estimation for downstream traffic would enable the use of low-power wireless for applications that require actuation in industrial applications.

\subsubsection{Industrial Process Control}

Industrial Process Control (IPC) is a production process where tasks are automated to optimize the efficiency of the process.
IPC applications require high reliability and low latency.
Schindler~\etal introduced the first characterization and implementation of a closed-loop wireless feedback control network using TSCH over a 4-hop network\cite{schindler17implementation}.
To the best of my knowledge, this is the first work on IPC using completely standards-compliant and TSCH-based network.
As IIoT matures, it tends toward applications that require service guarantees and predictable performances.

% % - - - - - - - - - - - - - - - - -
% \subsubsection{Security}

% - Sécurisation du réseau ou le petit paragraphe que tu avais prévu sur la sûreté de fonctionnement des réseaux (dependable networks).
% \lorem


% \subsubsection{Toward Dependable IoT}

% A dependable system is a system that can be depended upon. 
% In computer networks it can be defined as the combination of:
% \begin{itemize}
% \item \textbf{reliability}: the continuity of service given a set of expected requirements
% \item \textbf{availability}: the ability to provide a service at any given time
% \item \textbf{confidentiality}: the certitude that no unauthorized access to information is made
% \item \textbf{safety}: the absence of consequences on users and environments
% \item \textbf{integrity}: the absence of improper system or data alteration.
% \end{itemize}

% Dependable  networking  alone  is  insufficient.
% To guarantee the right operation of an IIoT system, the system needs to be taken into account as a whole.

% In this thesis we address the reliability and availability component 
% \lorem

